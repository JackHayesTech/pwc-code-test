import React, { useEffect, useState } from 'react';

import { getWeather } from './utilities';
import { WeatherApiResponse } from './WeatherContainer.types';
import {
  Location,
  Current,
  Advisory,
  Forecast,
  UnitToggle,
} from './components';
import { MeasurementUnits } from '../../data/Common';

export const WeatherContainer: React.FC = () => {
  const [weather, setWeather] = useState<WeatherApiResponse | null>(null);
  const [location, setLocation] = useState<String>('');
  const [temUnit, setTempUnit] = useState<MeasurementUnits>('M');

  useEffect(() => {
    if (!weather) {
      // TODO fail state.
      navigator.geolocation.getCurrentPosition(async (position) => {
        const { latitude, longitude } = position.coords;
        const weather = await getWeather(
          latitude.toString(),
          longitude.toString(),
        );
        setWeather(weather);
        setLocation(weather.timezone);
      });
    }
  });

  return weather ? (
    <div className="container mx-auto px-10 py-10">
      <div className="grid grid-cols-2">
        <div>
          <Location location={location} />
        </div>
        <div>
          <UnitToggle toggleUnit={setTempUnit} />
        </div>
        <div className="pb-10">
          <Current data={weather.current} mI={temUnit} />
        </div>
        <div>
          <Advisory data={weather.current} mI={temUnit} />
        </div>
        <div className="col-span-2">
          <Forecast forecast={weather.daily} mI={temUnit} />
        </div>
      </div>
      <div></div>
    </div>
  ) : (
    // TODO better loading.
    <div>'loading'</div>
  );
};
