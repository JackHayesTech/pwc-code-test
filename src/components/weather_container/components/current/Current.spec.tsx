import { render, screen } from '@testing-library/react';
import { Current } from './Current';

describe('Tests for the current component.', () => {
  const main = 'main';
  const icon = 'icon';
  const dt = 1607220000;
  const temp = 300;
  const mockData = {
    weather: [
      {
        main,
        icon,
      },
    ],
    dt,
    temp,
  } as any;

  it('Renders successfully with the metric system.', () => {
    render(<Current data={mockData} mI={'M'} />);

    expect(screen.getByTestId('date')).toHaveTextContent('December 6th 2020');
    expect(screen.getByTestId('main')).toHaveTextContent(main);
    expect(screen.getByTestId('temp')).toHaveTextContent('27 °C');

    expect(screen.getByTestId('img')).toHaveAttribute(
      'src',
      'http://openweathermap.org/img/wn/icon@2x.png',
    );
  });

  it('Renders successfully with the imperial system.', () => {
    render(<Current data={mockData} mI={'I'} />);
    expect(screen.getByTestId('temp')).toHaveTextContent('80 °F');
  });
});
