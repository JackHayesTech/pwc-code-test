import React from 'react';
import moment from 'moment';
import { CurrentWeather, convertTemperature, MeasurementUnits } from '..';

interface Props {
  data: CurrentWeather;
  mI: MeasurementUnits;
}

/**
 * Displays information about the current weather on the current location.
 * @param data
 */
export const Current: React.FC<Props> = ({ data, mI }) => {
  const { weather, temp, dt } = data;
  const { main, icon } = weather[0];
  const date = moment.unix(dt).format('MMMM Do YYYY');

  // TODO switch temperature
  return (
    <div>
      <div data-testid="date">{date.toString()}</div>
      <div data-testid="main">{main}</div>
      <div className="flex flex-col sm:flex-row">
        <div>
          <img
            data-testid="img"
            src={`http://openweathermap.org/img/wn/${icon}@2x.png`}
            alt="weather icon"
          />
        </div>
        <div className="text-3xl sm:text-5xl flex items-center">
          <div data-testid="temp">{convertTemperature(temp, mI)}</div>
        </div>
      </div>
    </div>
  );
};
