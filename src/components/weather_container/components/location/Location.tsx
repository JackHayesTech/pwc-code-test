import React from 'react';

interface Props {
  location: String;
}

/**
 * Has the location of the user.
 * TODO allow the user to enter in a new location that triggers new results.
 * @param location - the current location.
 */
export const Location: React.FC<Props> = ({ location }) => {
  return <div className="text-3xl pb-5">{location}</div>;
};
