import React from 'react';

import { DailyForecast, MeasurementUnits } from '..';
import { DaysForecast } from './DaysForecast';

interface Props {
  forecast: Array<DailyForecast>;
  mI: MeasurementUnits;
}

/**
 * Displays the weeks weather forecast.
 * @param forecast
 */
export const Forecast: React.FC<Props> = ({ forecast, mI }) => (
  <div className="grid grid-cols-2 sm:grid-cols-4 md:grid-cols-7">
    {forecast.slice(0, 7).map((day) => (
      <DaysForecast forecast={day} key={day.dt} mI={mI} />
    ))}
  </div>
);
