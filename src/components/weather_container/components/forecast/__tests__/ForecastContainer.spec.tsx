import { render, screen } from '@testing-library/react';
import { Forecast } from '../ForecastContainer';

jest.mock('../DaysForecast')

describe('Tests for the forecast component.', () => {
  const temp = Array(8)
  let forecast: any[];

  beforeEach(() => forecast = ([...temp] as any).map((e: number, i: number) => ({
    dt: i,
  })))

  it('Renders the correct number of child elements.', () => {
    render(<Forecast forecast={forecast} mI={'M'} />);
    expect(screen.getByTestId('0')).toBeInTheDocument();
    expect(screen.getByTestId('6')).toBeInTheDocument();
  });

});
