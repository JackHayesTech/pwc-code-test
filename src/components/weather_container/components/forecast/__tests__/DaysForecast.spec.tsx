import { render, screen } from '@testing-library/react';
import { DaysForecast } from '../DaysForecast';

describe('Tests for the days forecast component.', () => {
  const main = 'main';
  const icon = 'icon';
  const dt = 1607220000;
  const max = 30;
  const min = 20;
  const mockData = {
    weather: [
      {
        main,
        icon,
      },
    ],
    dt,
    temp: {
      max,
      min
    },
  } as any;

  it('Renders successfully with the metric system.', () => {
    render(<DaysForecast forecast={mockData} mI={'M'} />);

    expect(screen.getByTestId('date')).toHaveTextContent('Sunday');
    expect(screen.getByTestId('max')).toHaveTextContent('-243 °C');
    expect(screen.getByTestId('min')).toHaveTextContent('-253 °C');

    expect(screen.getByTestId('img')).toHaveAttribute(
      'src',
      'http://openweathermap.org/img/wn/icon@2x.png',
    );
  });

  it('Renders successfully with the imperial system.', () => {
    render(<DaysForecast forecast={mockData} mI={'I'} />);
    expect(screen.getByTestId('max')).toHaveTextContent('-406 °F');
    expect(screen.getByTestId('min')).toHaveTextContent('-424 °F');
  });
});
