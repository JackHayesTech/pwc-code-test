import React from 'react';
import moment from 'moment';

import { DailyForecast, convertTemperature, MeasurementUnits } from '..';

interface Props {
  forecast: DailyForecast;
  mI: MeasurementUnits;
}

/**
 * Displays forecast for a particular day.
 * @param forecast
 * @param mI
 */
export const DaysForecast: React.FC<Props> = ({ forecast, mI }) => {
  const { dt, weather, temp } = forecast;
  const { icon } = weather[0];
  const { max, min } = temp;
  const date = moment.unix(dt).format('dddd');

  return (
    <div className="text-center">
      <div>
        <span data-testid="date">{date}</span>
      </div>
      <div>
        <img
          data-testid="img"
          src={`http://openweathermap.org/img/wn/${icon}@2x.png`}
          alt="weather icon"
          className="m-auto"
        />
      </div>
      <div>
        <div>
          <span data-testid="max" className="font-bold">
            {convertTemperature(max, mI)}&#176;
          </span>{' '}
          <span data-testid="min" className="font-light">
            {convertTemperature(min, mI)}
          </span>
          &#176;
        </div>
      </div>
    </div>
  );
};
