import { render, screen } from '@testing-library/react';
import { Advisory } from './Advisory';

describe('Tests for the advisory component.', () => {
  const mockData = {
    humidity: 10,
    clouds: 20,
    pressure: 30,
    wind_speed: 40,
  } as any;

  it('Renders successfully with the metric system.', () => {
    render(<Advisory data={mockData} mI={'M'} />);
    expect(screen.getAllByRole('listitem')).toHaveLength(4);
    expect(screen.getByTestId('humidity')).toHaveTextContent('Humidity: 10%');
    expect(screen.getByTestId('cloudiness')).toHaveTextContent(
      'Cloudiness: 20%',
    );
    expect(screen.getByTestId('pressure')).toHaveTextContent(
      'Pressure: 30 hPa',
    );
    expect(screen.getByTestId('wind')).toHaveTextContent('Wind: 144.00 KM/H');
  });

  it('Renders successfully with the imperial system.', () => {
    render(<Advisory data={mockData} mI={'I'} />);
    expect(screen.getByTestId('wind')).toHaveTextContent('Wind: 89.48 MPH');
  });
});
