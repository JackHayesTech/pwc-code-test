import React from 'react';

import { CurrentWeather, MeasurementUnits, convertSpeed } from '..';

interface Props {
  data: CurrentWeather;
  mI: MeasurementUnits;
}

/**
 * Provides more in depth info on the current weather
 * @param data
 * @param mI
 */
export const Advisory: React.FC<Props> = ({ data, mI }) => {
  const { humidity, clouds, pressure, wind_speed } = data;

  return (
    <div>
      <ul>
        <li data-testid="humidity">Humidity: {humidity}%</li>
        <li data-testid="cloudiness">Cloudiness: {clouds}%</li>
        <li data-testid="pressure">Pressure: {pressure} hPa</li>
        <li data-testid="wind">Wind: {convertSpeed(wind_speed, mI)}</li>
      </ul>
    </div>
  );
};
