import React, { useState } from 'react';
import { Toggle } from 'react-toggle-component';

interface Props {
  toggleUnit: Function;
}

/**
 * Has the location of the user.
 * TODO allow the user to enter in a new location that triggers new results.
 * @param location - the current location.
 */
export const UnitToggle: React.FC<Props> = ({ toggleUnit }) => {
  const [on, setToggle] = useState<boolean>(false);

  const toggle = () => {
    setToggle((value) => !value);
    if (on) toggleUnit('I');
    else toggleUnit('M');
  };

  return (
    <label htmlFor="toggle-1">
      Metric
      <Toggle name="toggle-1" onToggle={toggle} />
      Imperial
    </label>
  );
};
