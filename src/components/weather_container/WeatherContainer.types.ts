interface Temperature {
  min: number;
  max: number;
}

interface Weather {
  main: String;
  description: String;
  icon: String;
}

export interface DailyForecast {
  dt: number;
  weather: Array<Weather>;
  temp: Temperature;
}

export interface CurrentWeather {
  dt: number;
  temp: number;
  weather: Array<Weather>;
  humidity: number;
  clouds: number;
  pressure: number;
  wind_speed: number;
}

export interface WeatherApiResponse {
  timezone: String;
  current: CurrentWeather;
  daily: Array<DailyForecast>;
}
