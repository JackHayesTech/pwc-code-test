import { Fetch, Method, WeatherApiResponse } from '..';

/**
 * Returns the weather data from the api
 * @param lat
 * @param lon
 */
export const getWeather = async (
  lat: string,
  lon: string,
): Promise<WeatherApiResponse> => {
  const fetch = new Fetch();
  const appid = process.env.REACT_APP_WEATHER_API_KEY;

  fetch.params = {
    appid,
    lat,
    lon,
  };

  const res = await fetch.send(
    'https://api.openweathermap.org/data/2.5/onecall',
    Method.GET,
  );

  return res as WeatherApiResponse;
};
