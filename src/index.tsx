import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { WeatherContainer } from './components';

ReactDOM.render(
  <React.StrictMode>
    <WeatherContainer />
  </React.StrictMode>,
  document.getElementById('root'),
);
