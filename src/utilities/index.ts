export { Fetch } from './fetch/Fetch';
export { Method } from './fetch/Fetch.interface';

export { convertTemperature } from './convert_temperature/ConvertTemperature';
export { convertSpeed } from './convert_speed/ConvertSpeed';

export * from '../data/Common.d'