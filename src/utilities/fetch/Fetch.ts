import { Headers, Method } from './Fetch.interface';

/**
 * Universal class for react frontends used to make api requests to jason apis.
 */
export class Fetch {
  private _headers: Headers = {};
  private _params = '';
  private _body: string | null = null;

  /**
   * Sets the requests headers.
   * @param headers - The headers to send to the api.
   */
  set headers(headers: Headers) {
    this._headers = headers;
  }

  /**
   * Sets the request parameters.
   * @param params - The request parameters.
   */
  set params(params: object) {
    this._params = '?';
    Object.entries(params).forEach(
      ([key, value]) => (this._params = `${this._params}${key}=${value}&`),
    );
    // Removes the trailing & symbol from the string.
    this._params = this._params.slice(0, -1);
  }

  /**
   * Sets the request body.
   * @param body - The request data to send to the api.
   */
  set body(body: object) {
    this._body = JSON.stringify(body);
  }

  /**
   *
   * @param url - The url of the service we are requesting data from.
   * @param method - The http method we are using to return the data.
   */
  public send = async (url: string, method: Method): Promise<unknown> => {
    // Constructs the request.
    const res = await fetch(`${url}${this._params}`, {
      method,
      headers: this._headers,
      body: this._body,
    });

    // An error occurred so throw the res object so the relevant data can be extracted.
    if (!res.ok) throw res;

    // No error occurred so return the data.
    const json = await res.json();
    return json;
  };
}
