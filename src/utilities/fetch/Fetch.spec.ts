import { Fetch } from './Fetch';
import { Method } from './Fetch.interface';
import fetchMock from 'jest-fetch-mock';

describe('Tests for the fetch class.', () => {
  let fetch: Fetch;

  beforeEach(() => {
    fetch = new Fetch();
    fetchMock.doMock();
  });

  it('Should set the header variable.', () => {
    const header = { header: 'header' };
    fetch.headers = header;
    expect(fetch['_headers']).toEqual(header);
  });

  it('Should set the params variable.', () => {
    const param1 = 'data1';
    const param2 = 'data2';
    const expected = `?param1=${param1}&param2=${param2}`;

    const params = {
      param1,
      param2,
    };

    fetch.params = params;

    expect(fetch['_params']).toEqual(expected);
  });

  it('Should set the body variable.', () => {
    const body = { data: 'test' };
    fetch.body = body;
    expect(fetch['_body']).toEqual(JSON.stringify(body));
  });

  it('Should call the fetch method with the expected parameters.', async () => {
    fetchMock.mockResponseOnce(JSON.stringify({ data: 'data' }));
    const url = 'url';
    const params = 'params';
    const body = { bod: 'body' };
    const headers = { head: 'test' };
    const method = Method.POST;
    const expectedFullUrl = `${url}?${params}=${params}`;
    const expectedRequest = { body: JSON.stringify(body), headers, method };

    fetch.params = { params };
    fetch.body = body;
    fetch.headers = headers;

    await fetch.send(url, method);
    expect(fetchMock).toHaveBeenCalledWith(expectedFullUrl, expectedRequest);
  });

  it('Should throw an error on fetch failure.', async () => {
    const thrownError = new Error('fake error message');
    fetchMock.mockReject(thrownError);
    let error;
    try {
      await fetch.send('url', Method.GET);
    } catch (err) {
      error = err;
    }
    expect(error).toEqual(thrownError);
  });
});
