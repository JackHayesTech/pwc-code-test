/**
 * The headers object for the fetch class.
 */
export interface Headers {
  [key: string]: string;
}

/**
 * Defines the http request methods.
 */
export enum Method {
  POST = 'post',
  GET = 'get',
  PUT = 'put',
  PATCH = 'patch',
  DELETE = 'delete',
}
