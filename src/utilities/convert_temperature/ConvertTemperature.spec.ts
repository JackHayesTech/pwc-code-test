import { convertTemperature } from './ConvertTemperature';

describe('Tests for the convert temperature function.', () => {
  const k = 300;
  it('Should return the expected temperature in celsius.', () => {
    const c = convertTemperature(k, 'M');
    expect(c).toEqual('27 °C');
  });

  it('Should return the expected temperature in fahrenheit.', () => {
    const f = convertTemperature(k, 'I');
    expect(f).toEqual('80 °F');
  });
});
