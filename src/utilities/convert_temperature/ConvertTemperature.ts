import { MeasurementUnits } from '..';

/**
 * Converts a kelvin temperature to either celsius or fahrenheit.
 * @param kelvin - Temperature to be converted.
 * @param conversion
 */
export const convertTemperature = (
  kelvin: number,
  conversion: MeasurementUnits,
): string => {
  let temp: number;
  let symbol = conversion === 'I' ? 'F' : 'C';

  // Convert to celsius.
  if (conversion === 'M') temp = kelvin - 273.15;
  // Convert to fahrenheit.
  else temp = (kelvin * 9) / 5 - 459.67;

  return `${Math.round(temp)} °${symbol}`;
};
