import { MeasurementUnits } from '..';

/**
 * Converts a meters/second to either km/hour or miles/hour.
 * @param speed - Speed to be converted.
 * @param conversion
 */
export const convertSpeed = (
  speed: number,
  conversion: MeasurementUnits,
): string => {
  let con: number;
  let symbol = conversion === 'I' ? 'MPH' : 'KM/H';

  // Convert to celsius.
  if (conversion === 'M') con = speed * 3.6;
  // Convert to fahrenheit.
  else con = speed * 2.237;

  return `${con.toFixed(2)} ${symbol}`;
};
