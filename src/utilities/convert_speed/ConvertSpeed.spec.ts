import { convertSpeed } from './ConvertSpeed';

describe('Tests for the convert temperature function.',() => {
  const s = 10;
  it('Should return the expected temperature in celsius.',() => {
    const c = convertSpeed(s, 'M');
    expect(c).toEqual('36.00 KM/H');
  })

  it('Should return the expected temperature in fahrenheit.',() => {
    const f = convertSpeed(s, 'I');
    expect(f).toEqual('22.37 MPH');
  })
});