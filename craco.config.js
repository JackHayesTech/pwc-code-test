module.exports = {
  compilerOptions: {
    "jsx": "react",
  },
  devServer:{
    proxy: {
      '/api': 'http://localhost:5000'
    }
  },
  style: {
    postcss: {
      plugins: [
        require('tailwindcss'),
        require('autoprefixer'),
      ],
    },
  },
}